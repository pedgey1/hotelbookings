﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BookingService
{
    public class BookingManager : IBookingManager
    {
        protected volatile IList<Room> Rooms;

        public BookingManager()
        {
            Rooms = new List<Room>();
            Rooms.Add(new Room(101));
            Rooms.Add(new Room(102));
            Rooms.Add(new Room(201));
            Rooms.Add(new Room(203));
        }

        public void AddBooking(string guest, int roomNumber, DateTime date)
        {
            date = StripTimeFromDate(date);
            var room = Rooms.SingleOrDefault(o => o.RoomNumber == roomNumber);
            if(room == null)
            {
                throw new RoomDoesntExistException();
            }
            lock(room) // Commenting out this line will cause concurrency unit tests to fail
            {
                if (!IsRoomAvailable(roomNumber, date))
                {
                    throw new RoomUnavailableException("Room Already Booked");
                }
                // If we are a debug build sleep (this is for unit tests)
#if DEBUG // could replace with NUnit Assembly reflection
            Thread.Sleep(1000);
#endif
            room.Bookings.Add(new Booking { Date = date, Guest = guest });
            }
        }

        public IEnumerable<int> getAvailableRooms(DateTime date)
        {
            date = StripTimeFromDate(date);
            return Rooms.Where(o => !o.Bookings.Any(p => p.Date == date)).Select(o => o.RoomNumber);
        }

        public bool IsRoomAvailable(int roomNumber, DateTime date)
        {
            date = StripTimeFromDate(date);
            var room = Rooms.Single(o => o.RoomNumber == roomNumber);
            return !room.Bookings.Any(o => o.Date == date);
        }

        /// <summary>
        /// Removes the hours/minutes/seconds/ms components of date time, reducing it to a date
        /// </summary>
        /// <param name="date">Date to strip</param>
        /// <returns>Date midnight of date</returns>
        private DateTime StripTimeFromDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day);
        }
    }
}
