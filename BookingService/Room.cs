﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookingService
{
    public class Room
    {
        public int RoomNumber;
        public bool IsBooked;
        public IList<Booking> Bookings;

        public Room(int roomNumber)
        {
            RoomNumber = roomNumber;
            IsBooked = false;
            Bookings = new List<Booking>();
        }
    }

    public struct Booking
    {
        public DateTime Date;
        public String Guest;
    }
}