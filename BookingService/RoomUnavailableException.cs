﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookingService
{
    public class RoomUnavailableException : Exception
    {
        public RoomUnavailableException(string message) : base(message)
        {
        }
    }
}
