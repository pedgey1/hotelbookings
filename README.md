## Status
![alt text](https://gitlab.com/pedgey/hotelbookings/badges/master/pipeline.svg "Status")

## Introduction

This is a simple .NET Core application, with unit tests for a Hotel Booking system.

## How is it built/how do I run it

# In Visual Studio

Using visual studio 2019 you can run the tests from the Test command, or in the Terminal navigate to the Tests directory and run `dotnet test`

# Gitlab CI Info

First, we use the official Microsoft .NET SDK image
to build our project.

```
image: microsoft/dotnet:latest
```

We're defining two stages here: `build`, and `test`.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```