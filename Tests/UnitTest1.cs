using BookingService;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void CanBookRoom()
        {
            // ARRANGE
            var bookingManager = new BookingManager();
            DateTime now = DateTime.Now;

            // ACT
            bookingManager.AddBooking("test", 101, now);

            // ASSERT
            Assert.False(bookingManager.IsRoomAvailable(101, now));
        }

        [Test]
        public void CannotBookRoomAlreadyBooked()
        {
            // ARRANGE
            var bookingManager = new BookingManager();
            DateTime now = DateTime.Now;

            // ACT
            bookingManager.AddBooking("test", 101, now);

            // ASSERT
            Assert.Throws(typeof(RoomUnavailableException), () => bookingManager.AddBooking("test2", 101, now));
        }


        [Test]
        public void CannotBookRoomAlreadyBookedMultiThreadedAsync()
        {
            // ARRANGE
            var bookingManager = new StubBookingManager();
            DateTime now = DateTime.Now;
            int bookingAttempts = 1000;

            // ACT
            //bookingManager.AddBooking("test", 101, now); 
            CountdownEvent cde = new CountdownEvent(bookingAttempts);
            int bookedCount = 0;
            Action consumer = () =>
            {
                // decrement CDE count once for each element consumed from queue
                while (bookingAttempts > 0)
                {
                    cde.Signal();
                    bookingAttempts--;
                    try
                    {
                        bookingManager.AddBooking("test", 101, now);
                        Console.WriteLine($"Booked {0}", cde.CurrentCount);
                        bookedCount++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Could not book {0} {1}", cde.CurrentCount, e.Message);
                    }                    
                }
            };

            // Start 10 Threads
            Task t1 = Task.Factory.StartNew(consumer);
            Task t2 = Task.Factory.StartNew(consumer);
            Task t3 = Task.Factory.StartNew(consumer);
            Task t4 = Task.Factory.StartNew(consumer);
            Task t5 = Task.Factory.StartNew(consumer);
            Task t6 = Task.Factory.StartNew(consumer);
            Task t7 = Task.Factory.StartNew(consumer);
            Task t8 = Task.Factory.StartNew(consumer);
            Task t9 = Task.Factory.StartNew(consumer);
            Task t10 = Task.Factory.StartNew(consumer);

            cde.Wait();

            Task.WhenAll(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);

            // ASSERT
            Assert.That(bookingManager.GetBookingsForDay(101, now).Count, Is.EqualTo(1));
            Assert.That(bookedCount, Is.EqualTo(1));
        }

        [Test]
        public void ThrowExceptionIfRoomDoesntExist()
        {
            // ARRANGE
            var bookingManager = new BookingManager();
            DateTime now = DateTime.Now;

            // ACT

            // ASSERT
            Assert.Throws(typeof(RoomDoesntExistException), () => bookingManager.AddBooking("test2", 109, now));
        }

        [Test]
        public void CanGetAllAvailableRooms()
        {
            // ARRANGE
            var bookingManager = new BookingManager();
            DateTime now = DateTime.Now;

            // ACT
            bookingManager.AddBooking("test", 101, now);

            // ASSERT
            Assert.That(bookingManager.getAvailableRooms(now).Count,Is.EqualTo(3));
        }
    }

    class StubBookingManager : BookingManager
    {
        public IList<Booking> GetBookingsForDay(int roomNumber, DateTime date)
        {
            var room = Rooms.Single(o => o.RoomNumber == roomNumber);
            return room.Bookings;
        }
    }
}